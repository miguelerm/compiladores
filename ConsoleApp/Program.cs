﻿using Compiladores.Servicios;
using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args == null || args.Length != 1)
            {
                Console.WriteLine("Debe especificar la ruta del archivo!");
                return;
            }

            var servicio = new GramaticaParser();
            var gramatica = servicio.ObtenerDesdeArchivo(args[0]);

            if (gramatica != null)
            {
                Console.WriteLine("======== Variables ========");
                foreach (var variable in gramatica.Variables)
                {
                    Console.WriteLine(" - {0}", variable);
                }

                Console.WriteLine("======== Terminales ========");
                foreach (var terminal in gramatica.Terminales)
                {
                    Console.WriteLine(" - {0}", terminal);
                }
            }
            else
            {
                Console.WriteLine("No se pudo generar una gramatica desde el archivo especificado.");
            }

            Console.ReadLine();
        }
    }
}
