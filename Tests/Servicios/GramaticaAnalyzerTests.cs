﻿using Compiladores.Entidades;
using Compiladores.Servicios;
using NUnit.Framework;
using System.Linq;

namespace Tests.Servicios
{
    [TestFixture]
    public class GramaticaAnalyzerTests
    {
        [Test]
        public void Primero_ConReglasDefinidas_RetornaSoloTerminales()
        {

            var reglas = CrearReglas();

            var servicio = new GramaticaAnalyzer();

            var primeroDeA = servicio.Primero(reglas, new Variable("A"));
            var primeroDeAprima = servicio.Primero(reglas, new Variable("A'"));
            var primeroDeB = servicio.Primero(reglas, new Variable("B"));
            var primeroDeC = servicio.Primero(reglas, new Variable("C"));
            var primeroDeD = servicio.Primero(reglas, new Variable("D"));

            Assert.That(primeroDeA.Select(x => x.Key).Distinct(), Is.EqualTo(new [] { new Terminal(null), new Terminal("a"), new Terminal("b"), new Terminal("c"), new Terminal("d"), new Terminal("f") }));
            Assert.That(primeroDeAprima.Select(x => x.Key).Distinct(), Is.EqualTo(new [] { new Terminal(null), new Terminal("a") }));
            Assert.That(primeroDeB.Select(x => x.Key).Distinct(), Is.EqualTo(new [] { new Terminal(null), new Terminal("b") }));
            Assert.That(primeroDeC.Select(x => x.Key).Distinct(), Is.EqualTo(new [] { new Terminal(null), new Terminal("c") }));
            Assert.That(primeroDeD.Select(x => x.Key).Distinct(), Is.EqualTo(new [] { new Terminal(null), new Terminal("c"), new Terminal("d"), new Terminal("f") }));
        }

        [Test]
        public void Siguiente_ConReglasDefinidas_RetornaSoloTerminales()
        {
            var reglas = CrearReglas();

            var servicio = new GramaticaAnalyzer();

            var siguienteDeA = servicio.Siguiente(reglas, new Variable("A"));
            var siguienteDeAprima = servicio.Siguiente(reglas, new Variable("A'"));
            var siguienteDeB = servicio.Siguiente(reglas, new Variable("B"));
            var siguienteDeC = servicio.Siguiente(reglas, new Variable("C"));
            var siguienteDeD = servicio.Siguiente(reglas, new Variable("D"));

            Assert.That(siguienteDeA.Select(x => x.Key).Distinct(), Is.EqualTo(new[] { new Terminal("$") }));
            Assert.That(siguienteDeAprima.Select(x => x.Key).Distinct(), Is.EqualTo(new[] { new Terminal("$") }));
            Assert.That(siguienteDeB.Select(x => x.Key).Distinct(), Is.EqualTo(new[] { new Terminal("$"), new Terminal("c") }));
            Assert.That(siguienteDeC.Select(x => x.Key).Distinct(), Is.EqualTo(new[] { new Terminal("$"), new Terminal("c"), new Terminal("d"), new Terminal("f") }));
            Assert.That(siguienteDeD.Select(x => x.Key).Distinct(), Is.EqualTo(new[] { new Terminal("$"), new Terminal("a") }));
        }

        [Test]
        public void Siguiente_ConGramaticaDeOperaciones_RetornaSoloTerminales()
        {
            var gramatica = CrearGramaticaOperacionesAritmeticas();

            var servicio = new GramaticaAnalyzer();

            var siguienteDeE = servicio.Siguiente(gramatica.Reglas, new Variable("E"));
            var siguienteDeEPrima = servicio.Siguiente(gramatica.Reglas, new Variable("E'"));
            var siguienteDeT = servicio.Siguiente(gramatica.Reglas, new Variable("T"));
            var siguienteDeTPrima = servicio.Siguiente(gramatica.Reglas, new Variable("T'"));
            var siguienteDeF = servicio.Siguiente(gramatica.Reglas, new Variable("F"));

            Assert.That(siguienteDeE.Select(x => x.Key).Distinct().ToArray(), Is.EqualTo(new[] { new Terminal("$"), new Terminal(")") }));
            Assert.That(siguienteDeEPrima.Select(x => x.Key).Distinct().ToArray(), Is.EqualTo(new[] { new Terminal("$"), new Terminal(")") }));
            Assert.That(siguienteDeT.Select(x => x.Key).Distinct().ToArray(), Is.EqualTo(new[] { new Terminal("$"), new Terminal(")"), new Terminal("-"), new Terminal("+") }));
            Assert.That(siguienteDeTPrima.Select(x => x.Key).Distinct(), Is.EqualTo(new[] { new Terminal("$"), new Terminal(")"), new Terminal("-"), new Terminal("+") }));
            Assert.That(siguienteDeF.Select(x => x.Key).Distinct().ToArray(), Is.EqualTo(new[] { new Terminal("$"), new Terminal(")"), new Terminal("*"), new Terminal("-"), new Terminal("/"), new Terminal("+") }));
        }

        [Test]
        public void GenerarTablaSintactica_ConGramaticaSinAmbiguedad_GeneraLaTablaCorrecta()
        {

            var gramatica = CrearGramaticaOperacionesAritmeticas();

            var servicio = new GramaticaAnalyzer();

            var tabla = servicio.GenerarTablaSintactica(gramatica);

            Assert.That(tabla.Filas.ElementAt(0).Variable, Is.EqualTo(new Variable("E")));
            Assert.That(tabla.Filas.ElementAt(1).Variable, Is.EqualTo(new Variable("T")));
            Assert.That(tabla.Filas.ElementAt(2).Variable, Is.EqualTo(new Variable("F")));
            Assert.That(tabla.Filas.ElementAt(3).Variable, Is.EqualTo(new Variable("E'")));
            Assert.That(tabla.Filas.ElementAt(4).Variable, Is.EqualTo(new Variable("T'")));

            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(0).Terminal, Is.EqualTo(new Terminal("+")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(1).Terminal, Is.EqualTo(new Terminal("-")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(2).Terminal, Is.EqualTo(new Terminal("*")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(3).Terminal, Is.EqualTo(new Terminal("/")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(4).Terminal, Is.EqualTo(new Terminal("(")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(5).Terminal, Is.EqualTo(new Terminal(")")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(6).Terminal, Is.EqualTo(new Terminal("id")));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(7).Terminal, Is.EqualTo(new Terminal("$")));


            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(0).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(1).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(2).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(3).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(4).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("E"), Produccion = new[] { new Variable("T"), new Variable("E'") } } }));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(5).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(6).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("E"), Produccion = new[] { new Variable("T"), new Variable("E'") } } }));
            Assert.That(tabla.Filas.ElementAt(0).Columnas.ElementAt(7).Reglas, Is.Empty);

            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(0).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(1).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(2).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(3).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(4).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T"), Produccion = new[] { new Variable("F"), new Variable("T'") } } }));
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(5).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(6).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T"), Produccion = new[] { new Variable("F"), new Variable("T'") } } }));
            Assert.That(tabla.Filas.ElementAt(1).Columnas.ElementAt(7).Reglas, Is.Empty);

            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(0).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(1).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(2).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(3).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(4).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("F"), Produccion = new object[] { new Terminal("("), new Variable("E"), new Terminal(")") } } }));
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(5).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(6).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("F"), Produccion = new[] { new Terminal("id") } } }));
            Assert.That(tabla.Filas.ElementAt(2).Columnas.ElementAt(7).Reglas, Is.Empty);

            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(0).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("E'"), Produccion = new object[] { new Terminal("+"), new Variable("T"), new Variable("E'") } } }));
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(1).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("E'"), Produccion = new object[] { new Terminal("-"), new Variable("T"), new Variable("E'") } } }));
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(2).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(3).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(4).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(5).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("E'"), Produccion = new object[] { new Terminal(null) } } }));
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(6).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(3).Columnas.ElementAt(7).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("E'"), Produccion = new object[] { new Terminal(null) } } }));

            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(0).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T'"), Produccion = new object[] { new Terminal(null) } } }));
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(1).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T'"), Produccion = new object[] { new Terminal(null) } } }));
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(2).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T'"), Produccion = new object[] { new Terminal("*"), new Variable("F"), new Variable("T'") } } }));
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(3).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T'"), Produccion = new object[] { new Terminal("/"), new Variable("F"), new Variable("T'") } } }));
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(4).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(5).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T'"), Produccion = new object[] { new Terminal(null) } } }));
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(6).Reglas, Is.Empty);
            Assert.That(tabla.Filas.ElementAt(4).Columnas.ElementAt(7).Reglas, Is.EqualTo(new object[] { new ReglaGramatical { Variable = new Variable("T'"), Produccion = new object[] { new Terminal(null) } } }));
        }

        private static Gramatica CrearGramaticaOperacionesAritmeticas()
        {
            var gramatica = new Gramatica
            {
                Variables = new[] { 
                    new Variable("E"), 
                    new Variable("T"),
                    new Variable("F"),
                    new Variable("E'"), 
                    new Variable("T'") 
                },
                Terminales = new[] { 
                    new Terminal("+"),
                    new Terminal("-"),
                    new Terminal("*"),
                    new Terminal("/"),
                    new Terminal("("),
                    new Terminal(")"),
                    new Terminal("id"),
                    new Terminal(null)
                },
                Reglas = new[] { 
                    new ReglaGramatical{
                        Variable = new Variable("E"),
                        Produccion = new object [] {                            
                            new Variable("T"),
                            new Variable("E'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T"),
                        Produccion = new object [] {
                            new Variable("F"),
                            new Variable("T'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("F"),
                        Produccion = new object [] {
                            new Terminal("("),
                            new Variable("E"),
                            new Terminal(")") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("F"),
                        Produccion = new object [] {
                            new Terminal("id") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("E'"),
                        Produccion = new object [] {
                            new Terminal("+"),
                            new Variable("T"),
                            new Variable("E'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("E'"),
                        Produccion = new object [] {
                            new Terminal("-"),
                            new Variable("T"),
                            new Variable("E'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("E'"),
                        Produccion = new object [] {
                            new Terminal(null)
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T'"),
                        Produccion = new object [] {
                            new Terminal("*"),
                            new Variable("F"),
                            new Variable("T'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T'"),
                        Produccion = new object [] {
                            new Terminal("/"),
                            new Variable("F"),
                            new Variable("T'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T'"),
                        Produccion = new object [] {
                            new Terminal(null) 
                        }
                    }
                }
            };
            return gramatica;
        }

        private static ReglaGramatical[] CrearReglas()
        {
            var reglas = new[]{
                new ReglaGramatical {
                    Variable = new Variable("A"),
                    Produccion = new object[] {
                        new Variable("B"),
                        new Variable("C"),
                        new Variable("D"),
                        new Variable("A'")
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("A'"),
                    Produccion = new object[] {
                        new Terminal("a"),
                        new Variable("A'")
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("A'"),
                    Produccion = new object[] {
                        new Terminal(null)
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("B"),
                    Produccion = new object[] {
                        new Terminal("b")
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("B"),
                    Produccion = new object[] {
                        new Terminal(null)
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("C"),
                    Produccion = new object[] {
                        new Terminal("c")
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("C"),
                    Produccion = new object[] {
                        new Terminal(null)
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("D"),
                    Produccion = new object[] {
                        new Terminal("d")
                    }
                },
                new ReglaGramatical {
                    Variable = new Variable("D"),
                    Produccion = new object[] {
                        new Variable("C"),
                        new Terminal("f")
                    }
                }
            };
            return reglas;
        }
    }
}
