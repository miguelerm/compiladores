﻿
using Compiladores.Entidades;
using Compiladores.Servicios;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;

namespace Tests.Servicios
{
    [TestFixture]
    public class GramaticaParserTests
    {

        const string gramatica_001 = @"A:'a'B
A:'b'A
B:'a'B
B:'b'C
B:'b'
C:'a'B
C:'b'B";

        const string gramatica_002 = @"A:'a'B|'b'A
B:'a'B|'b'C|'b'
C:'a'B|'b'B";

        private static string ObtenerRuta(string archivo)
        {
            var assemblyLocation = typeof(GramaticaParser).Assembly.Location;
            var assemblyPath = Path.GetDirectoryName(assemblyLocation);

            return Path.Combine(assemblyPath, archivo);
        }

        [Test]
        public void ObtenerDesdeArchivo_ConReglasVerticales_RetornaUnaGramaticaValida()
        {

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { @"C:\gramatica-001.txt", new MockFileData(gramatica_001) }
            });

            var servicio = new GramaticaParser(fileSystem);
            var A = new Variable("A");
            var B = new Variable("B");
            var C = new Variable("C");
            var a = new Terminal("a");
            var b = new Terminal("b");
            
            var gramatica = servicio.ObtenerDesdeArchivo(@"C:\gramatica-001.txt");

            Assert.IsNotNull(gramatica);
            Assert.AreEqual(3, gramatica.Variables.Count);
            Assert.AreEqual(2, gramatica.Terminales.Count);

            Assert.AreEqual(new Variable("A"), gramatica.Variables.ElementAt(0));
            Assert.AreEqual(new Variable("B"), gramatica.Variables.ElementAt(1));
            Assert.AreEqual(new Variable("C"), gramatica.Variables.ElementAt(2));

            Assert.AreEqual(new Terminal("a"), gramatica.Terminales.ElementAt(0));
            Assert.AreEqual(new Terminal("b"), gramatica.Terminales.ElementAt(1));
        }

        [Test]
        public void ObtenerDesdeArchivo_ConReglasVerticales_RetornaLasProduccionesEsperadas()
        {

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { @"C:\gramatica-001.txt", new MockFileData(gramatica_001) }
            });

            var servicio = new GramaticaParser(fileSystem);
            var gramatica = servicio.ObtenerDesdeArchivo(@"C:\gramatica-001.txt");

            Assert.AreEqual(7, gramatica.Reglas.Count);

            Assert.AreEqual(new Variable("A"), gramatica.Reglas.ElementAt(0).Variable);
            Assert.AreEqual(new Variable("A"), gramatica.Reglas.ElementAt(1).Variable);
            Assert.AreEqual(new Variable("B"), gramatica.Reglas.ElementAt(2).Variable);
            Assert.AreEqual(new Variable("B"), gramatica.Reglas.ElementAt(3).Variable);
            Assert.AreEqual(new Variable("B"), gramatica.Reglas.ElementAt(4).Variable);
            Assert.AreEqual(new Variable("C"), gramatica.Reglas.ElementAt(5).Variable);
            Assert.AreEqual(new Variable("C"), gramatica.Reglas.ElementAt(6).Variable);

            Assert.That(gramatica.Reglas.ElementAt(0).Produccion, Is.EquivalentTo(new object[] { new Terminal("a"), new Variable("B") }));
            Assert.That(gramatica.Reglas.ElementAt(1).Produccion, Is.EquivalentTo(new object[] { new Terminal("b"), new Variable("A") }));
            Assert.That(gramatica.Reglas.ElementAt(2).Produccion, Is.EquivalentTo(new object[] { new Terminal("a"), new Variable("B") }));
            Assert.That(gramatica.Reglas.ElementAt(3).Produccion, Is.EquivalentTo(new object[] { new Terminal("b"), new Variable("C") }));
            Assert.That(gramatica.Reglas.ElementAt(4).Produccion, Is.EquivalentTo(new object[] { new Terminal("b") }));
            Assert.That(gramatica.Reglas.ElementAt(5).Produccion, Is.EquivalentTo(new object[] { new Terminal("a"), new Variable("B") }));
            Assert.That(gramatica.Reglas.ElementAt(6).Produccion, Is.EquivalentTo(new object[] { new Terminal("b"), new Variable("B") }));
        }

        [Test]
        public void ObtenerDesdeArchivo_ConReglasHorizontales_RetornaUnaGramaticaValida()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { @"C:\gramatica-002.txt", new MockFileData(gramatica_002) }
            });

            var servicio = new GramaticaParser(fileSystem);
            var A = new Variable("A");
            var B = new Variable("B");
            var C = new Variable("C");
            var a = new Terminal("a");
            var b = new Terminal("b");

            var gramatica = servicio.ObtenerDesdeArchivo(@"C:\gramatica-002.txt");

            Assert.IsNotNull(gramatica);
            Assert.AreEqual(3, gramatica.Variables.Count);
            Assert.AreEqual(2, gramatica.Terminales.Count);

            Assert.AreEqual(new Variable("A"), gramatica.Variables.ElementAt(0));
            Assert.AreEqual(new Variable("B"), gramatica.Variables.ElementAt(1));
            Assert.AreEqual(new Variable("C"), gramatica.Variables.ElementAt(2));

            Assert.AreEqual(new Terminal("a"), gramatica.Terminales.ElementAt(0));
            Assert.AreEqual(new Terminal("b"), gramatica.Terminales.ElementAt(1));
        }

    }
}
