﻿using Compiladores.Entidades;
using Compiladores.Servicios;
using NUnit.Framework;

namespace Tests.Servicios
{
    [TestFixture]
    public class GramaticaNormalizerTests
    {

        private Gramatica CrearGramaticaNoRecursiva()
        {
            var gramatica = new Gramatica
            {
                Variables = new[] { 
                    new Variable("A"), 
                    new Variable("B"),
                    new Variable("C")
                },
                Terminales = new[] { 
                    new Terminal("a"),
                    new Terminal("b")
                },
                Reglas = new[] { 
                    new ReglaGramatical{
                        Variable = new Variable("A"),
                        Produccion = new object [] {
                            new Terminal("a"),
                            new Variable("B") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("A"),
                        Produccion = new object [] {
                            new Terminal("b"),
                            new Variable("A") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("B"),
                        Produccion = new object [] {
                            new Terminal("a"),
                            new Variable("B") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("B"),
                        Produccion = new object [] {
                            new Terminal("b"),
                            new Variable("C") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("B"),
                        Produccion = new object [] {
                            new Terminal("a") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("C"),
                        Produccion = new object [] {
                            new Terminal("a"),
                            new Variable("B") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("C"),
                        Produccion = new object [] {
                            new Terminal("b"),
                            new Variable("B") 
                        }
                    }
                }
            };

            return gramatica;
        }

        private Gramatica CrearGramaticaRecursivaParaOperaciones()
        {
            var gramatica = new Gramatica
            {
                Variables = new[] { 
                    new Variable("E"), 
                    new Variable("T"),
                    new Variable("F")
                },
                Terminales = new[] { 
                    new Terminal("+"),
                    new Terminal("*"),
                    new Terminal("a"),
                    new Terminal("("),
                    new Terminal(")")
                },
                Reglas = new[] { 
                    new ReglaGramatical{
                        Variable = new Variable("E"),
                        Produccion = new object [] {
                            new Variable("E"),
                            new Terminal("+"),
                            new Variable("T")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("E"),
                        Produccion = new object [] {
                            new Variable("T") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T"),
                        Produccion = new object [] {
                            new Variable("T"),
                            new Terminal("*"),
                            new Variable("F") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T"),
                        Produccion = new object [] {
                            new Variable("F") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("F"),
                        Produccion = new object [] {
                            new Terminal("a") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("F"),
                        Produccion = new object [] {
                            new Terminal("("),
                            new Variable("E"),
                            new Terminal(")") 
                        }
                    }
                }
            };

            return gramatica;
        }

        private Gramatica CrearGramaticaNoRecursivaParaOperaciones()
        {
            var gramatica = new Gramatica
            {
                Variables = new[] { 
                    new Variable("E"), 
                    new Variable("T"),
                    new Variable("F"),
                    new Variable("E'"), 
                    new Variable("T'") 
                },
                Terminales = new[] { 
                    new Terminal("+"),
                    new Terminal("*"),
                    new Terminal("a"),
                    new Terminal("("),
                    new Terminal(")"),
                    new Terminal(null)
                },
                Reglas = new[] { 
                    new ReglaGramatical{
                        Variable = new Variable("E"),
                        Produccion = new object [] {                            
                            new Variable("T"),
                            new Variable("E'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T"),
                        Produccion = new object [] {
                            new Variable("F"),
                            new Variable("T'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("F"),
                        Produccion = new object [] {
                            new Terminal("a") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("F"),
                        Produccion = new object [] {
                            new Terminal("("),
                            new Variable("E"),
                            new Terminal(")") 
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("E'"),
                        Produccion = new object [] {
                            new Terminal("+"),
                            new Variable("T"),
                            new Variable("E'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("E'"),
                        Produccion = new object [] {
                            new Terminal(null)
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T'"),
                        Produccion = new object [] {
                            new Terminal("*"),
                            new Variable("F"),
                            new Variable("T'")
                        }
                    },
                    new ReglaGramatical{
                        Variable = new Variable("T'"),
                        Produccion = new object [] {
                            new Terminal(null) 
                        }
                    }
                }
            };

            return gramatica;
        }

        [Test]
        public void EliminarRecursividadIzquierda_EnGramaticasNoRecursivas_NoRealizaCambios()
        {
            var gramatica = CrearGramaticaNoRecursiva();
            var gramaticaEsperada = CrearGramaticaNoRecursiva();

            var servicio = new GramaticaNormalizer();

            servicio.EliminarRecursividadIzquierda(gramatica);

            Assert.That(gramatica.Variables, Is.EqualTo(gramaticaEsperada.Variables));
            Assert.That(gramatica.Terminales, Is.EqualTo(gramaticaEsperada.Terminales));
            Assert.That(gramatica.Reglas, Is.EqualTo(gramaticaEsperada.Reglas));
        }

        [Test]
        public void EliminarRecursividadIzquierda_EnGramaticasRecursivas_EliminarRecursividad()
        {
            var gramatica = CrearGramaticaRecursivaParaOperaciones();
            var gramaticaEsperada = CrearGramaticaNoRecursivaParaOperaciones();

            var servicio = new GramaticaNormalizer();

            servicio.EliminarRecursividadIzquierda(gramatica);

            Assert.That(gramatica.Variables, Is.EqualTo(gramaticaEsperada.Variables));
            Assert.That(gramatica.Terminales, Is.EqualTo(gramaticaEsperada.Terminales));
            Assert.That(gramatica.Reglas, Is.EqualTo(gramaticaEsperada.Reglas));
        }

    }
}
