﻿using Compiladores.Entidades;
using Compiladores.Servicios;
using NUnit.Framework;

namespace Tests.Servicios
{
    [TestFixture]
    public class ProduccionParserTests
    {
        [Test]
        public void ObtenerProduccion_ConUnaTerminal_RetornaUnaTerminal()
        {
            var esperado = new object[] { new Terminal("b") };
            var parser = new ProduccionParser();
            var gramatica = new Gramatica();

            var resultado = parser.ObtenerProduccion(gramatica, "'b'");

            Assert.That(resultado, Is.EquivalentTo(esperado));
        }

        [Test]
        public void ObtenerProduccion_ConUnaTerminalYUnaVariable_RetornaUnaConcatenacion()
        {
            var esperado = new object[] { new Terminal("b"), new Variable("A") };
            var parser = new ProduccionParser();
            var gramatica = new Gramatica();

            var resultado = parser.ObtenerProduccion(gramatica, "'b'A");

            Assert.That(resultado, Is.EquivalentTo(esperado));
        }

        [Test]
        public void ObtenerProduccion_ConVariableDeNombreLargo_InterpretaCorrectamenteElNombreLargo()
        {
            var esperado = new object[] { new Terminal("b"), new Variable("ABC"), new Terminal("c") };
            var parser = new ProduccionParser();
            var gramatica = new Gramatica();

            var resultado = parser.ObtenerProduccion(gramatica, "'b'ABC'c'");

            Assert.That(resultado, Is.EquivalentTo(esperado));
        }

        [Test]
        public void ObtenerProduccion_ConReglasComplejas_RetornaUnaConcatenacionConVariosElementos()
        {
            var esperado = new object[] { new Terminal("if"), new Variable("CONDICION"), new Terminal("then") };
            var parser = new ProduccionParser();
            var gramatica = new Gramatica();

            var resultado = parser.ObtenerProduccion(gramatica, "'if' CONDICION 'then'");

            Assert.That(resultado, Is.EquivalentTo(esperado));
        }
    }
}
