﻿using Compiladores.Entidades;
using Compiladores.Servicios;
using Nustache.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class MainForm : Form
    {
        private readonly AnalizadorSintactico analizador = new AnalizadorSintactico();

        const string plantilla = "UI.Plantilla.html";

        public MainForm()
        {
            InitializeComponent();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarArchivo();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            BuscarArchivo();
        }

        private void BuscarArchivo()
        {
            var resultado = ofdArchivo.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                var ruta = ofdArchivo.FileName;

                try
                {
                    analizador.Cargar(ruta);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error al leer el archivo: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Renderizar(analizador.ObtenerGramaticaOriginal(), null, null);
                    return;
                }

                try
                {
                    analizador.Procesar();
                    Renderizar(analizador.ObtenerGramaticaOriginal(), analizador.ObtenerGramaticaNormalizada(), analizador.ObtenerTablaSintactica());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error al procesar la gramática: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Renderizar(analizador.ObtenerGramaticaOriginal(), null, null);
                    return;
                }


                
            }

        }

        private void Renderizar(Gramatica gramaticaOriginal, Gramatica gramaticaNormalizada, TablaSintactica tablaSintactica)
        {
            var template = "";
            using (var sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(plantilla)))
            {
                template = sr.ReadToEnd();
            }

            ICollection<ColumnaSintactica> columnas = null;
            if (tablaSintactica != null)
            {
                columnas = tablaSintactica.Filas.First().Columnas;
            }

            var html = Render.StringToString(template, new { 
                GramaticaOriginal = gramaticaOriginal, 
                GramaticaNormalizada = gramaticaNormalizada,
                TablaSintactica = tablaSintactica,
                Columnas = columnas
            });

            webBrowser1.Navigate("about:blank");
            Application.DoEvents();
            var doc = webBrowser1.Document.OpenNew(false);

            doc.Write(html);
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new AboutBox();
            dlg.ShowDialog();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

     
    }
}
