﻿using Compiladores.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiladores.Servicios
{
    public class AnalizadorSintactico
    {
        private readonly GramaticaParser parser = new GramaticaParser();
        private readonly GramaticaNormalizer normalizer = new GramaticaNormalizer();
        private readonly GramaticaAnalyzer analyser = new GramaticaAnalyzer();
        

        private Gramatica gramaticaOriginal = null;
        private Gramatica gramatica = null;
        private TablaSintactica tabla = null;



        public void Cargar(string ruta)
        {
            gramaticaOriginal = parser.ObtenerDesdeArchivo(ruta);
            gramatica = parser.ObtenerDesdeArchivo(ruta);
        }


        public void Procesar()
        {
            try
            {
                normalizer.EliminarRecursividadIzquierda(gramatica);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió un error al eliminar la recursividad: " + ex.Message, ex);
            }

            try
            {
                analyser.CalcularPrimero(gramatica);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió un error al calcular la función primera: " + ex.Message, ex);
            }

            try
            {
                analyser.CalcularSiguiente(gramatica);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió un error al calcular la función siguiente: " + ex.Message, ex);
            }

            try
            {
                tabla = analyser.GenerarTablaSintactica(gramatica);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió un error al generar la tabla sintáctica: " + ex.Message, ex);
            }

        }


        public Gramatica ObtenerGramaticaOriginal()
        {
            return gramaticaOriginal;
        }

        public Gramatica ObtenerGramaticaNormalizada()
        {
            return gramatica;
        }

        public TablaSintactica ObtenerTablaSintactica()
        {
            return tabla;
        }
    }
}
