﻿using Compiladores.Entidades;
using System.Linq;

namespace Compiladores.Servicios
{
    public class GramaticaNormalizer
    {
        public void EliminarRecursividadIzquierda(Gramatica gramatica)
        {

            foreach (var variable in gramatica.Variables.ToArray())
            {

                var reglasRecursivas = gramatica.Reglas.Where(x => x.Variable.Equals(variable) && x.EsRecursivaPorLaIzquierda()).ToList();
                var reglasNoRecursivas = gramatica.Reglas.Where(x => x.Variable.Equals(variable) && !x.EsRecursivaPorLaIzquierda()).ToList();

                if (reglasRecursivas.Any() && reglasNoRecursivas.Any())
                {
                    // Si existen al menos dos reglas, una de la forma A -> Aα (recursiva por la izquierda)
                    // y otra de la forma A -> β (que no sea recursiva por la izquierda):
                    //
                    //      A -> Aα | β
                    //
                    // entonces:

                    // - Se crea una nueva variable con el mismo nombre mas concatenando un apostrofe (A').
                    var variablePrima = gramatica.AgregarVariable(variable.Nombre + "'");

                    foreach (var regla in reglasRecursivas)
                    {
                        // - por cada regla recursiva se hace lo siguiente:

                        // * Se toma la última parte de la producción.
                        var producciones = regla.Produccion.Skip(1).ToList();

                        // * Se concatena al final de la producción la nueva variable creada.
                        producciones.Add(variablePrima);

                        // * Se crea la nueva regla (A' -> αA')
                        gramatica.AgregarRegla(variablePrima, producciones);
                    }

                    // - Se crea la producción hacia epsilon (A' -> ϵ)
                    var epsilon = gramatica.AgregarTerminal(null);
                    gramatica.AgregarRegla(variablePrima, new object[] { epsilon });

                    // - Se eliminan todas las reglas recursivas (de la forma A -> Aα)
                    reglasRecursivas.ForEach(x => gramatica.Reglas.Remove(x));

                    foreach (var regla in reglasNoRecursivas)
                    {
                        // - Se le concatena la variable prima (A') a cada regla no recursiva de la variable A.
                        //   así estas reglas quedan de la forma A -> βA'
                        regla.Concatenar(variablePrima);
                    }
                }
            }
        }
    }
}
