﻿using Compiladores.Entidades;
using Seterlund.CodeGuard;
using System;
using System.IO;
using System.IO.Abstractions;
using System.Linq;

namespace Compiladores.Servicios
{
    /// <summary>
    /// Clase que permite leer la especificación de una gramatica desde un archivo de texto plano.
    /// </summary>
    public class GramaticaParser
    {
        /// <summary>
        /// Componente que permite acceder al sistema de archivos.
        /// </summary>
        private readonly IFileSystem fileSystem;

        /// <summary>
        /// Componente que permite interpretar producciones desde cadenas de texto.
        /// </summary>
        private readonly ProduccionParser produccionParser;

        /// <summary>
        /// Constructor por defecto de la clase.
        /// </summary>
        public GramaticaParser()
        {
            this.fileSystem = new FileSystem();
            this.produccionParser = new ProduccionParser();
        }

        /// <summary>
        /// Sobrecarga del constructor de la clase para sustituir las dependencias.
        /// </summary>
        /// <param name="fileSystem">Sistema de archivos</param>
        public GramaticaParser(IFileSystem fileSystem)
        {
            Guard.That(fileSystem, "fileSystem").IsNotNull();
            this.fileSystem = fileSystem;
            this.produccionParser = new ProduccionParser();
        }

        /// <summary>
        /// Lee la especificación de una gramática desde el archivo ubicado en <paramref name="ruta"/>.
        /// </summary>
        /// <param name="ruta">Ruta en la que se encuentra ubicado el archivo.</param>
        /// <returns>Retorna la gramática construida según la especificación del archivo.</returns>
        public Gramatica ObtenerDesdeArchivo(string ruta)
        {
            if (!fileSystem.File.Exists(ruta))
            {
                throw new FileNotFoundException("El archivo especificado no existe", fileSystem.Path.GetFileName(ruta));
            }

            var gramatica = new Gramatica();

            using (var reader = new StreamReader(fileSystem.File.OpenRead(ruta)))
            {
                var indice = 0;

                while (!reader.EndOfStream)
                {
                    indice++;
                    var linea = reader.ReadLine();

                    if (string.IsNullOrWhiteSpace(linea)) continue;

                    var partes = linea.Split(':');

                    if (partes.Length != 2)
                    {
                        throw new InvalidOperationException(string.Format("La línea {0} no es válida, se esperaban dos puntos (:).", indice));
                    }
                    else
                    {
                        var nombreVariable = partes[0].Trim();

                        var variable = gramatica.AgregarVariable(nombreVariable);

                        var textoProducciones = partes[1].Trim();

                        foreach (var regla in textoProducciones.Split('|'))
                        {
                            var produccion = produccionParser.ObtenerProduccion(gramatica, regla);
                            if (produccion.Count() > 0)
                            {
                                gramatica.AgregarRegla(variable, produccion);
                            }
                        }
                    }
                }
            }

            return gramatica;
        }
    }
}
