﻿using Compiladores.Entidades;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Compiladores.Servicios
{
    /// <summary>
    /// Clase que brinda la funcionalidad necesaria para formar las producciones de una regla gramatical con base en una cadena de texto.
    /// </summary>
    public class ProduccionParser
    {
        /// <summary>
        /// Expresion regular que permite separar los terminales de las variables en una cadena de caracteres.
        /// </summary>
        private readonly Regex expresionRegular = new Regex(@"'(?<terminales>.+?)'|(?<epsilon>e)|(?<variables>\w+)", RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled | RegexOptions.CultureInvariant);

        public ICollection<object> ObtenerProduccion(Gramatica gramatica, string produccion)
        {
            var concatenacion = new List<object>();

            var matches = expresionRegular.Matches(produccion);

            foreach (Match match in matches)
            {
                if (match.Success)
                {
                    var grupoTerminales = match.Groups["terminales"];
                    var grupoVariables = match.Groups["variables"];
                    var grupoEpsilon = match.Groups["epsilon"];

                    if (grupoTerminales.Success)
                    {
                        var terminal = gramatica.AgregarTerminal(grupoTerminales.Value);
                        concatenacion.Add(terminal);
                    }

                    if (grupoVariables.Success)
                    {
                        var variable = gramatica.AgregarVariable(grupoVariables.Value);
                        concatenacion.Add(variable);
                    }

                    if (grupoEpsilon.Success)
                    {
                        var epsilon = gramatica.AgregarTerminal(null);
                        concatenacion.Add(epsilon);
                    }
                }
            }

            return concatenacion;
        }
    }
}
