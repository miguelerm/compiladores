﻿using Compiladores.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Compiladores.Servicios
{
    public class GramaticaAnalyzer
    {
        private const int IteracionesMaximas = 1000;
        private readonly Terminal epsilon = new Terminal(null);

        public IEnumerable<KeyValuePair<Terminal, ReglaGramatical>> Primero(IEnumerable<ReglaGramatical> reglas, Variable variable)
        {
            return PrimeroSeguro(reglas, variable, 0);

        }

        private IEnumerable<KeyValuePair<Terminal, ReglaGramatical>> PrimeroSeguro(IEnumerable<ReglaGramatical> reglas, Variable variable, int iteraciones)
        {
            if (iteraciones > IteracionesMaximas)
            {
                throw new StackOverflowException(string.Format("Existen reglas recursivas o que sobrepasan los {0} niveles de anidado.", IteracionesMaximas));
            }

            var resultado = new List<KeyValuePair<Terminal, ReglaGramatical>>();

            // Se obtienen todas las producciones que genera la variable que se está analizando.
            var reglasIncluidas = reglas.Where(x => x.Variable.Equals(variable)).ToArray();

            foreach (var regla in reglasIncluidas)
            {
                // Por cada una de las producciones que genere la variable, se calcula el "primero".
                foreach (var primero in regla.Produccion)
                {
                    if (primero is Terminal)
                    {
                        // Si el primero es un terminal, se agrega el terminal al conjunto de primeros de la variable.
                        resultado.Add(new KeyValuePair<Terminal, ReglaGramatical>(primero as Terminal, regla));
                        break;
                    }
                    else if (primero is Variable)
                    {
                        // si primero es una variable, se calcula el primero de esta nueva variable y se
                        // agrega el resultado como primeros de la variable que se está analizando.
                        var primeros = PrimeroSeguro(reglas, primero as Variable, iteraciones + 1).ToList();

                        foreach (var item in primeros.ToArray())
                        {
                            var item2 = new KeyValuePair<Terminal, ReglaGramatical>(item.Key, regla);
                            primeros.Remove(item);
                            primeros.Add(item2);
                        }

                        resultado.AddRange(primeros);

                        // Si primero contiene epsilon, se calcula el primero del siguiente elemento de la
                        // producción, de lo contrario se finaliza el calculo del "primero".
                        if (!primeros.Any(x => x.Key.Equals(epsilon)))
                        {
                            break;
                        }
                    }
                }
            }

            // Se retorna el conjunto de elementos que están en primero de la variable.
            return resultado.Distinct().OrderBy(x => x.Key.ToString()).ToArray();
        }

        public IEnumerable<KeyValuePair<Terminal, ReglaGramatical>> Siguiente(IEnumerable<ReglaGramatical> reglas, Variable variable)
        {
            return SiguienteSeguro(reglas, variable, 0);
        }

        private IEnumerable<KeyValuePair<Terminal, ReglaGramatical>> SiguienteSeguro(IEnumerable<ReglaGramatical> reglas, Variable variable, int iteraciones)
        {
            if (iteraciones > IteracionesMaximas)
            {
                throw new StackOverflowException(string.Format("Existen reglas recursivas o que sobrepasan los {0} niveles de anidado.", IteracionesMaximas));
            }

            var resultado = new List<KeyValuePair<Terminal, ReglaGramatical>>();
            resultado.Add(new KeyValuePair<Terminal, ReglaGramatical>(new Terminal("$"), null));

            // Se obtienen todas las producciones en las que se encuentre la variable que se está analizando.
            var reglasIncluidas = reglas.Where(x => x.Produccion.Contains(variable)).ToArray();

            if (reglasIncluidas.Any())
            {
                foreach (var regla in reglasIncluidas)
                {
                    object anterior = null;
                    var analizar = false;
                    foreach (var elemento in regla.Produccion)
                    {
                        if (analizar)
                        {
                            if (elemento is Terminal)
                            {
                                // Si el primero es un terminal, se agrega el terminal al conjunto de primeros de la variable.
                                resultado.Add(new KeyValuePair<Terminal, ReglaGramatical>(elemento as Terminal, regla));
                                analizar = false;
                                break;
                            }

                            var primeroDeBeta = Primero(reglas, elemento as Variable).ToList();

                            foreach (var item in primeroDeBeta.ToArray())
                            {
                                var item2 = new KeyValuePair<Terminal, ReglaGramatical>(item.Key, regla);
                                primeroDeBeta.Remove(item);
                                primeroDeBeta.Add(item2);
                            }

                            resultado.AddRange(primeroDeBeta.Where(x => !x.Key.Equals(epsilon)).ToArray());

                            if (primeroDeBeta.Any(x => x.Key.Equals(epsilon)) && !regla.Variable.Equals(variable))
                            {
                                var siguienteDeA = SiguienteSeguro(reglas, regla.Variable, iteraciones + 1).ToList();

                                foreach (var item in siguienteDeA.ToArray())
                                {
                                    var item2 = new KeyValuePair<Terminal, ReglaGramatical>(item.Key, regla);
                                    siguienteDeA.Remove(item);
                                    siguienteDeA.Add(item2);
                                }

                                resultado.AddRange(siguienteDeA);
                            }

                            analizar = false;
                        }

                        if (elemento.Equals(variable))
                        {
                            analizar = true;
                        }

                        anterior = elemento;
                    }

                    if (analizar && !regla.Variable.Equals(variable))
                    {
                        var siguienteDeA = SiguienteSeguro(reglas, regla.Variable, iteraciones + 1).ToList();

                        foreach (var item in siguienteDeA.ToArray())
                        {
                            var item2 = new KeyValuePair<Terminal, ReglaGramatical>(item.Key, regla);
                            siguienteDeA.Remove(item);
                            siguienteDeA.Add(item2);
                        }

                        resultado.AddRange(siguienteDeA);
                    }  
                }
            }

            return resultado.Distinct().OrderBy(x => x.ToString()).ToArray();
        }

        public TablaSintactica GenerarTablaSintactica(Gramatica gramatica)
        {
            var tabla = new TablaSintactica();

            foreach (var variable in gramatica.Variables)
            {
                FilaSintactica fila = tabla.AgregarFila(variable);

                foreach (var terminal in gramatica.Terminales.Where(x => !x.Equals(epsilon)))
                {
                    fila.AgregarColumna(terminal);
                }

                fila.AgregarColumna(new Terminal("$"));

                var primeroDeA = Primero(gramatica.Reglas, variable);
                var siguienteDeA = Siguiente(gramatica.Reglas, variable);

                foreach (var terminal in primeroDeA)
                {
                    var columna = fila.AgregarColumna(terminal.Key);

                    columna.AgregarRegla(terminal.Value);
                   
                }

                if (primeroDeA.Any(x => x.Key.Equals(epsilon)))
                {
                    foreach (var terminal in siguienteDeA)
                    {
                        var columna = fila.AgregarColumna(terminal.Key);
                        columna.AgregarRegla(terminal.Value);
                    }

                    if (siguienteDeA.Any(x => x.Key.Equals(new Terminal("$"))))
                    {
                        foreach (var item in primeroDeA.Where(x => x.Key.Equals(epsilon)))
                        {
                            var columna = fila.AgregarColumna(new Terminal("$"));
                            columna.AgregarRegla(item.Value);
                        }
                    }
                }
            }

            return tabla;
        }

        public void CalcularPrimero(Gramatica gramatica)
        {
            foreach (var variable in gramatica.Variables)
            {
                variable.Primero = Primero(gramatica.Reglas, variable);
            }
        }

        public void CalcularSiguiente(Gramatica gramatica)
        {
            foreach (var variable in gramatica.Variables)
            {
                variable.Siguiente = Siguiente(gramatica.Reglas, variable);
            }
        }
    }
}
