﻿using System.Collections.Generic;
using System.Linq;

namespace Compiladores.Entidades
{
    public class Gramatica
    {
        public ICollection<Variable> Variables { get; set; }
        public ICollection<Terminal> Terminales { get; set; }
        public ICollection<ReglaGramatical> Reglas { get; set; }

        public Gramatica()
        {
            Variables = new HashSet<Variable>();
            Terminales = new HashSet<Terminal>();
            Reglas = new HashSet<ReglaGramatical>();
        }

        public Variable AgregarVariable(string nombre)
        {

            if (Variables.IsReadOnly)
            {
                Variables = Variables.ToList();
            }

            var variable = Variables.FirstOrDefault(x => x.Nombre == nombre);
            if (variable == null)
            {
                variable = new Variable(nombre);
                Variables.Add(variable);
            }
            return variable;
        }

        public Terminal AgregarTerminal(string nombreTerminal)
        {
            if (Terminales.IsReadOnly)
            {
                Terminales = Terminales.ToList();
            }

            var terminal = Terminales.FirstOrDefault(x => x.Nombre == (nombreTerminal ?? Terminal.Epsilon));

            if (terminal == null)
            {
                terminal = new Terminal(nombreTerminal);
                Terminales.Add(terminal);
            }

            return terminal;
        }

        public ReglaGramatical AgregarRegla(Variable variable, ICollection<object> produccion)
        {
            if (Reglas.IsReadOnly)
            {
                Reglas = Reglas.ToList();
            }

            if (!Variables.Contains(variable))
            {
                if (Variables.IsReadOnly)
                {
                    Variables = Variables.ToList();
                }

                Variables.Add(variable);
            }

            var regla = new ReglaGramatical { Variable = variable, Produccion = produccion };
            Reglas.Add(regla);

            return regla;
        }
    }
}
