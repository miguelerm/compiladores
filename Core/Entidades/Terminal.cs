﻿namespace Compiladores.Entidades
{
    public class Terminal
    {

        public const string Epsilon = "<epsilon>";

        public string Nombre { get; private set; }

        public Terminal(string nombre)
        {
            this.Nombre = nombre ?? Epsilon;
        }

        public override int GetHashCode()
        {
            return this.Nombre.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var that = obj as Terminal;

            if (that == null) return false;

            return string.Compare(this.Nombre, that.Nombre) == 0;
        }

        public override string ToString()
        {
            return this.Nombre;
        }

        public bool EsEpsilon
        {
            get
            {
                return Nombre == Epsilon;
            }
        }
    }
}
