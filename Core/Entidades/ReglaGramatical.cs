﻿using System.Collections.Generic;
using System.Linq;

namespace Compiladores.Entidades
{
    public class ReglaGramatical
    {
        public Variable Variable { get; set; }
        public ICollection<object> Produccion { get; set; }

        public ReglaGramatical()
        {
            this.Produccion = new List<object>();
        }

        public bool EsRecursivaPorLaIzquierda()
        {
            return this.Variable.Equals(this.Produccion.First());
        }

        public void Concatenar(object elemento)
        {
            if (Produccion.IsReadOnly)
            {
                Produccion = Produccion.ToList();
            }

            Produccion.Add(elemento);
        }

        public override bool Equals(object obj)
        {
            var that = obj as ReglaGramatical;

            if (that == null) return false;

            if (!Variable.Equals(that.Variable)) return false;

            if (Produccion.Count != that.Produccion.Count) return false;

            for (int i = 0; i < Produccion.Count; i++)
            {
                if (!Produccion.ElementAt(i).Equals(that.Produccion.ElementAt(i))) return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0} -> {1}", Variable, string.Join(" ", Produccion.Select(x => x.ToString())));
        }
    }
}
