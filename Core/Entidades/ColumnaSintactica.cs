﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Compiladores.Entidades
{
    public class ColumnaSintactica
    {
        public Terminal Terminal { get; set; }
        public ICollection<ReglaGramatical> Reglas { get; set; }

        public ColumnaSintactica(Terminal terminal)
        {
            this.Terminal = terminal;
            this.Reglas = new List<ReglaGramatical>();
        }

        public void AgregarRegla(ReglaGramatical reglaGramatical)
        {
            if (reglaGramatical != null && !Reglas.Contains(reglaGramatical))
            {
                Reglas.Add(reglaGramatical);
            }
        }
    }
}
