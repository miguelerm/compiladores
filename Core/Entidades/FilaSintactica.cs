﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Compiladores.Entidades
{
    public class FilaSintactica
    {
        public Variable Variable { get; set; }
        public ICollection<ColumnaSintactica> Columnas { get; set; }


        public FilaSintactica(Variable variable)
        {
            this.Variable = variable;
            Columnas = new List<ColumnaSintactica>();
        }

        public ColumnaSintactica AgregarColumna(Terminal terminal)
        {
            var columna = Columnas.FirstOrDefault(x => x.Terminal.Equals(terminal));

            if (columna == null)
            {
                columna = new ColumnaSintactica(terminal);
                Columnas.Add(columna);
            }

            return columna;
        }
    }
}
