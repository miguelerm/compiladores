﻿using Seterlund.CodeGuard;
using System.Collections.Generic;
using System.Linq;

namespace Compiladores.Entidades
{
    public class Variable
    {
        public string Nombre { get; private set; }

        public Variable(string nombre)
        {
            Guard.That(nombre, "nombre").IsNotNull();
            this.Nombre = nombre;
        }

        public override int GetHashCode()
        {
            return this.Nombre.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var that = obj as Variable;

            if (that == null) return false;

            return string.Compare(this.Nombre, that.Nombre) == 0;
        }

        public override string ToString()
        {
            return this.Nombre;
        }

        public IEnumerable<KeyValuePair<Terminal, ReglaGramatical>> Primero { get; set; }

        public IEnumerable<KeyValuePair<Terminal, ReglaGramatical>> Siguiente { get; set; }

        public IEnumerable<Terminal> PrimeroHelper
        {
            get
            {
                return Primero.Select(x => x.Key).Distinct().ToArray();
            }
        }

        public IEnumerable<Terminal> SiguienteHelper
        {
            get
            {
                return Siguiente.Select(x => x.Key).Distinct().ToArray();
            }
        }
    }
}
