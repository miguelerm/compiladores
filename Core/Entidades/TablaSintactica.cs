﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiladores.Entidades
{
    public class TablaSintactica
    {

        public ICollection<FilaSintactica> Filas { get; set; }

        public TablaSintactica()
        {
            Filas = new List<FilaSintactica>();
        }

        public FilaSintactica AgregarFila(Variable variable)
        {
            var fila = Filas.FirstOrDefault(x => x.Variable.Equals(variable));

            if (fila == null)
            {
                fila = new FilaSintactica(variable);
                Filas.Add(fila);
            }

            return fila;
        }
    }
}
